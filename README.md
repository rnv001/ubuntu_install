# This repo guides to install Ubuntu on Acer Predator PH315 #

0. Attach a second Screen to HDMI Port
1. At Boot press F2 to set a BIOS password so that you can disable Secureboot and FastBoot
2. Press `control+s` on main BIOS menu. change sata storage from RAID with optane to AHCI
3. Install Ubuntu 20.04 from a Stick or USB CD-ROM. Recommended config is as below

    a. `/`  > 80GB
	
    b. swap area > 10GB
	
    c. boot storage > 250MB
	
4. Install proprietary NVIDIA Driver with 
```sudo apt-get install nvidia-470```

## Issue 1: Second display cannot be connected
1. install Lightdm
```
sudo apt-get install -y lightdm
```
2. enable lightdm lock
    1. open settings>keyboard shortcuts> change the existing lock shortcut to a random shortcut 
    2. create a new custom shortcut lightdm lock with command `dm-tool lock` and shortcut windows+L 
	
## Issue 2. Machine cannot hibernate
1. install Grub customizer
`sudo apt-get install grub-customizer`
2. Follow the instruction as mentioned [here](https://askubuntu.com/questions/1240123/how-to-enable-hibernate-option-in-ubuntu-20-0).


Note: for Hibernation to work. You need to have a swap memory of atleast 8GB

## Issue 3. No volume on HDMI
1. Follow readme [here](https://github.com/hhfeuer/nvhda)

## Issue 4. Fan doesnt work on Ubuntu
1. Test the current sensors
```
sudo apt-get install lm-sensors
sudo sensors-detect
sensors
```
2. you should be able to find fan here. Else follow through with the next steps
	a. Install Mono and NBFC
```
sudo apt install git
git clone --depth 1 https://github.com/hirschmann/nbfc.git ~/Downloads/nbfc
# Build NBFC
sudo mkdir /opt/nbfc
sudo cp -r ~/Downloads/nbfc/Linux/bin/Release /opt/nbfc/
sudo cp ~/Downloads/nbfc/Linux/{nbfc.service,nbfc-sleep.service} /etc/systemd/system/
sudo systemctl enable nbfc --now
```
3. Copy the xml file in this repo and paste in /opt/nbfc/Configs
4. start the nbfc for the config
```
cd /opt/nbfc
mono nbfc.exe config --apply "Acer Predator PH315-53"
mono nbfc.exe start
mono nbfc.exe status --all
```

## Issue 5. High battery consumption on ubuntu
1. Follow the steps [here](https://github.com/matthieugras/Prime-Ubuntu-18.04)

## Issue 6. Windows doesnt boot
1. Open BIOS.
2. Go to main page
3. press `ctrl+s`
4. change SATA mode from AHCI back to Optane with RAID
